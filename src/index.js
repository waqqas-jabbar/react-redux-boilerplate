import React, {
  AppRegistry
} from 'react-native';

import {
  Provider,
} from 'react-redux';

import configureStore from './lib/configureStore';

import App from './components/App';

export default function native(platform) {

  function getInitialState() {
    const _initState = {
    };
    return _initState;
  }

  let Appl = React.createClass( {
    render() {

      const store = configureStore(getInitialState());

      // setup the router table with App selected as the initial component
      return (
        <Provider store={store}>
         <App />
        </Provider>
      );
    }
  });
  AppRegistry.registerComponent('app', () => Appl);
}
