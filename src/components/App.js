'use strict';

import React,
{
  Component,
  StyleSheet,
  View,
  Text
}
from 'react-native';


class App extends Component{

  render() {
    <View style={style.container}>
      <Text>Days of the week</Text>
    </View>
  }
}

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default App;
